﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IDataTransmitter
    {
        Task SendMessage(Message messageToSend, Guid conversationToSendMessageTo);

        Task SendUserInformation(List<User> userToSend);

        Task SendConversation(Conversation conversationToSend);
    }
}
