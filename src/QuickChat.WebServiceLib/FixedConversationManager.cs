﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    class FixedConversationManager : IConversationManager
    {
        //Guid here corrusponds to a Guid for a conversation
        Dictionary<Guid, Conversation> _allConversations;

        public FixedConversationManager(List<Conversation> allConversations, IUserManager manager)
        {
            _allConversations = allConversations.ToDictionary(key => key.ConversationID);
        }

        public Conversation GetConversationFromID(Guid conversationToGet)
        {
            //TODO handle error if conversation doesn't exist.
            return _allConversations[conversationToGet];
        }

        public bool DeleteConversation(Guid conversationToDelete, IUserManager userManager)
        {
            List<Guid> allUsersInConversation = _allConversations[conversationToDelete].UsersInConversation;
            foreach (Guid thisUser in allUsersInConversation)
            {
                userManager
                    .GetUserFromID(thisUser)
                    .RemoveConversationFromMyList(conversationToDelete);
            }
            return _allConversations.Remove(conversationToDelete);
        }

        public void AddNewConversation(Conversation newConversationToAdd)
        {
            _allConversations.Add(newConversationToAdd.ConversationID, newConversationToAdd);
        }

        public void AddMessageToConversation(string message, Guid conversationID, Guid userID)
        {
            _allConversations[conversationID].AddMessage(new Message(message, DateTime.Now, userID));
            //TODO implement user alerts for people getting message
        }

        public void RemoveUserFromConversation(Guid conversationID, Guid userID)
        {
            _allConversations[conversationID].RemoveUserFromConversation(userID);
        }

        public void RemoveUserFromAllConversations(Guid userID)
        {
            foreach (Conversation conversation in _allConversations.Values)
            {
                conversation.RemoveUserFromConversation(userID);
            }
        }
    }
}
