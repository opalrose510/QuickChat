﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IDataReceiver
    {
        List<Message> RetrieveNewMessages(Conversation conversationToGetInformationFrom, DateTime timeStamp);

        List<User> RetrieveUserInformation();

        Conversation RetrieveConversation(Guid conversationToGet);
    }
}
