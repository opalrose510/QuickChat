﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{

    public class Message
    {
        public string Contents { get; set; }
        public DateTime TimeStamp { get; set; }
        public Guid From { get; set; }
        public Guid Id { get; set; }

        //for use in manual creation of new message.
        public Message(string contents, DateTime timeStamp, Guid from)
        {
            Contents = contents;
            TimeStamp = timeStamp;
            From = from;
            Id = Guid.NewGuid();
        }

        //for use in serialization/deserialization via entity framework
        public Message()
        {
        }

    }
}
