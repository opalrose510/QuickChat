﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickchat.UI
{
    /// <summary>
    /// this could be a fairly expensive search, we may want to do this on the server side
    /// but in that case, we would just substitute the local implementation for one 
    /// that hits an endpoint 
    /// </summary>
    public interface IContactSearchEngine
    {
        List<ContactViewModel> SearchContactViewModels(List<ContactViewModel> contacts, string search);
    }
}
