﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using QuickChat.WebServiceLib;

namespace Quickchat.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<ContactViewModel> _tempContacts;
        private List<MessageViewModel> _tempMessages;
        private IDataTransmitter _transmitter;
        private IDataReceiver _receiver;

        public MainWindow()
        {
            InitializeComponent();
            _tempMessages = new List<MessageViewModel>
            {
                new MessageViewModel(DateTime.Now)
                {
                    From = "test",
                    Message = "Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages?",
                },
                new MessageViewModel(DateTime.Now)
                {
                    From = "test",
                    Message = "Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages?",
                },
                new MessageViewModel(DateTime.Now)
                {
                    From = "test",
                    Message = "Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages?",
                },
                new MessageViewModel(DateTime.Now)
                {
                    From = "test",
                    Message = "Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages?",
                },
                new MessageViewModel(DateTime.Now)
                {
                    From = "test",
                    Message = "Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages? Hey, I was just checking to see if you got the latest messages?",
                }

            };
            Console.WriteLine("Test Start\n");
            Message myMessage = new Message("Patrick to Evan, do you read me?", DateTime.Now, new Guid());
            FixedDataTransmitter myTransmitter = new FixedDataTransmitter();
            myTransmitter.SendMessage(myMessage, new Guid());
            Console.WriteLine("Test End\n");
            MessagesList.ItemsSource = _tempMessages;
            _receiver = new FixedDataReceiver("fixedUsers.json");
            _transmitter = new FixedDataTransmitter("fixedMessages.json");
            
            var users = _receiver.RetrieveUserInformation();

            

        }

        private void ContactsSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            IContactSearchEngine engine = new LocalNameContactSearch();
            ContactsList.ItemsSource = engine.SearchContactViewModels(_tempContacts, ContactsSearch.Text);
            ContactsScroll.ScrollToTop();
        }
    }
}
