﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickchat.UI
{
    public interface IMessageSearchEngine
    {
        List<MessageViewModel> SearchMessageViewModels(List<MessageViewModel> messages);
    }
}
