﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Quickchat.UI
{
    /// <summary>
    /// This class has extensions for various objects to allow smooth/easy functionality, so we don't have to repeat kloogy and error prone code. 
    /// </summary>
    public static class Extensions
    {
        public static void SetText(this RichTextBox richTextBox, Run text)
        {
            richTextBox.Document.Blocks.Clear();
            richTextBox.Document.Blocks.Add(new Paragraph(text));
        }

        public static void SetTextRaw(this RichTextBox richTextBox, Run text)
        {
            richTextBox.Document.Blocks.Clear();
            richTextBox.Document.Blocks.Add(new Paragraph(text));
        }

        public static TextRange GetText(this RichTextBox richTextBox)
        {
            return new TextRange(richTextBox.Document.ContentStart,
                richTextBox.Document.ContentEnd);
        }

        public static string GetTextRaw(this RichTextBox richTextBox)
        {
            return new TextRange(richTextBox.Document.ContentStart,
                richTextBox.Document.ContentEnd).Text;
        }
    }
}
